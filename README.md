### Use this repository for your local environment

### Attention. 
> For using this repository you need to have [Traefik 2.0 repo.](https://bitbucket.org/don-ostapenko/docker-traefik-2.0/src/master/)
___

#### 1 step.
* Clone this repository, if you didn't do it before.
```
$ git clone https://don-ostapenko@bitbucket.org/don-ostapenko/docker-docker-skeleton.git
```

#### 2 step.
* Edit **.env** (e.g. change *project-name* and *project-base-url*.) and **docker-compose.yml** files.
* It might be helpful to use **docker-compose.override.yml** for your custom configuration. Remove part of name "example" from **docker-compose.override.example.yml** and Docker be ready to use it.

#### 3 step.
* Run your containers by next command:
```
$ docker-compose up -d
```

#### Useful tips.
* If you want change a port of e.g. mariadb service, don't forget specified a port that don't crossing with other databases ports that look into the world on your machine. By the way, change a **DB_PORT** in the **.env** file.
* If you adding new service, specify **labels** and **networks**.
For services that will be have domain, labels must be contain:
```
labels:
  - "traefik.enable=true"
  - "traefik.http.routers.${PROJECT_NAME}_servise_name.entrypoints=https"
  - "traefik.http.routers.${PROJECT_NAME}_servise_name.rule=Host(`subdomain.${PROJECT_BASE_URL}`)"
  - "traefik.http.routers.${PROJECT_NAME}_servise_name.tls=true"
  - "traefik.http.routers.${PROJECT_NAME}_servise_name.tls.certresolver=letsEncrypt"
  - "traefik.docker.network=traefik-network"
networks:
  - traefik-network
  - internal
```
Where:
* `_servise_name` is name of your new service;
* `subdomain` desired name of subdomain;

For other only networks:
```
networks:
  - internal
```